#!/usr/bin/env bash
# ---------------------------------------------------------
# Projeto  : rss-tube
# Arquivo  : rss-tube.sh
# Descrição:
# Versão   : 1.0
# Data     : 08/04/2021 - 21:09
# Autor    : Altair Costa <altaircosta@gmail.com>
# Licença  : GNU/GPL v3.0
# ---------------------------------------------------------
# Execução : rss-tube.sh ou ./rss-tube.sh
# ---------------------------------------------------------

# Nome do canal=variável armazena ID=ID do canal

# Requisitos não contemplados:
# 1. Execução fornecendo o nome como argumento
# 2. Mensagens de erros

# ---------------------------------------------------------

# Leitura do arquivo sources

for a in {1..5}
    do 
        nome[$a]="$(sed -n "$a p" sources | cut -d'=' -f1)"
        canal[$a]="$(sed -n "$a p" sources | cut -d'=' -f2)"
        uid[$a]="$(sed -n "$a p" sources | cut -d'=' -f3)"
     done

# Link para o RSS dos canais do youtube
# https://www.youtube.com/feeds/videos.xml?channel_id=ID_DO_CANAL
rss_canal='https://www.youtube.com/feeds/videos.xml?channel_id='
#
## string inicial link do vídeo
link_video='https://www.youtube.com/watch?v='
#
# links dos canais escolhidos para testes
# Curso GNU - Kretcheu
# Curso GNU=video_krt=UCQTTe8puVKqurziI6Do-H-Q

# debxp - Blau
# Debxp=video_blu=UC8EGrwe_DXSzrCQclf_pv9g

# Dr Sax Love - Smooth Jazz
# Dr.SaxLove=video_sax=UCNJFXYXkXt_P8bJUxb21MpA

# University of California Television - UCT
# University of California Television=video_uct=UCh6KFtW4a4Ozr81GI1cxaBQ
# WR Kits - Wagner Rambo
# WR Kits=video_wrk=UCazAvTtoRlOrFDWDJDB2DKQ

# variável que armazena id do canal
canal_id=

clear


# Função de reprodução
reproduz() {
    mpv $1 
}

submenu () {
  local PS3='Escolha um vídeo: '
  local escolhas=("${title[1]}" "${title[2]}" "${title[3]}" "${title[4]}" "${title[5]}" "Volta ao menu principal")
  local opt
  select opt in "${escolhas[@]}"
  do
      case $opt in
          "${title[1]}")
              reproduz "${video[1]}" &
              kill -9 $(pgrep -f rss-tube.sh)
              ;;
          "${title[2]}")
              reproduz "${video[2]}" &
              kill -9 $(pgrep -f rss-tube.sh)
              ;;
          "${title[3]}")
              reproduz "${video[3]}" &
              kill -9 $(pgrep -f rss-tube.sh)
              ;;
          "${title[4]}")
              reproduz "${video[4]}" &
              kill -9 $(pgrep -f rss-tube.sh)
              ;;
          "${title[5]}")
              reproduz "${video[5]}" &
              kill -9 $(pgrep -f rss-tube.sh)
              ;;
          "Volta ao menu principal")
              return
              ;;
          *) echo "Opção inválida $REPLY"
      esac
      break
  done
}

# Menu principal
PS3='Escolha um canal: '
escolhas=("${nome[1]}" "${nome[2]}" "${nome[3]}" "${nome[4]}" "${nome[5]}" "Sair")
select canal_id in "${escolhas[@]}"
do
    case $canal_id in
    "${nome[1]}")
        canal_id="${uid[1]}"
        ;;
    "${nome[2]}")
        canal_id="${uid[2]}"
        ;;
    "${nome[3]}")
        canal_id="${uid[3]}"
        ;;
    "${nome[4]}") 
        canal_id="${uid[4]}"
        ;;
    "${nome[5]}")
        canal_id="${uid[5]}"
        ;;
    "Sair")
        break
        ;;
    *) echo "Opção inválida $REPLY"
    esac
        break
done

# Salvando em arquivo, as linhas com a string dos títulos, eliminando as tags e caracteres indesejados
sed -n '/^.*<media:title>.*$/p' <<< "$(wget  $rss_canal$canal_id  -q -O -)" |\
sed '{s/<[^>]*>//g ; s/^ *//g ; s/&quot;/\"/g;}' | head -n5  > titulos

# Salvando em arquivo, as linhas com a string dos links, eliminando as tags e caracteres indesejados
sed -n '/^.*<yt:videoId>.*$/p' <<< "$(wget $rss_canal$canal_id  -q -O -)" |\
sed '{s/<[^>]*>//g ; s/^ *//g;}' | head -n5 > links

# Criação, a partir do arquivo titulos, do vetor title com os nomes dos vídeos
for a in {1..5}
    do
        title[$a]="$(sed -n "$a p" titulos)"
    done

# Criação, a partir do arquivo links, do vetor video com os endereços dos vídeos
for a in {1..5}
    do
        video[$a]="$link_video""$(sed -n "$a p" links)"
    done
echo

# Removendo os arquivos temporários

rm links titulos

submenu
