```
#!/usr/bin/env bash

__FILE__=$(readlink -f "${BASH_SOURCE[0]}")
SCRIPT_NAME=${__FILE__##*/}

function String.help(){
  local _usage="$SCRIPT_NAME CARACTER STRING"
  printf '%s\n  Uso: %s\n' \
    'Necessário informar os parâmetros obrigatórios' \
    "$_usage"
  return 1
}

#/**
# * Função que formata a saída da seguinte forma
# * $ String.format X teste
# * XXXXXXXXX
# * X teste X
# * XXXXXXXXX
# *
# * Funcionamento:
# * A função recebe os parâmetros posicionais $1 e $2 e os armazena nas variáveis
# * locais c e str.
# * Caso o usuário envie mais do que 1 caracter em $1, c recebe apenas o primeiro
# * caracter enviado.
# * Verifica se c e str não são vazios, caso uma das 2 seja, retorna
# * mensagem de uso do script.
# * Calcula o comprimento de str e soma 4 (caracteres espaço e c em volta de texto
# * na segunda linha da saída) atribuindo na variável length
# * Length é o comprimento de caracteres a ser utilizado para formatar as primeira
# * e última linha da saída. Isto é feito com o printf '%Ns' que produz uma string
# * com N espaços, e printf -v padding, atribui a padding esse valor de string com
# * N espaços.
# * Caso o caracter enviado em $1 não seja ' ' (espaço), padding=${padding// /$c}
# * substitui através de uma expansão bash, todos os espaços que printf devolveu por
# * caracter na variável c
# * Por fim, printf imprime a saída no formato especificado,
# * a string dentro da "caixa" no formato de caracteres
# *
# * @param  string $1 single caracter
# * @param  string $2 string input
# * @return string dentro da "caixa" de caracteres
# */
function String.format() {
  local c=${1::1} str=$2
  [[ $c ]] && [[ $str ]] || String.help
  length=$((${#str}+4))
  printf -v padding "%${length}s"
  [[ $c == ' ' ]] || padding=${padding// /$c}
  printf '%s\n%s %s %s\n%s\n' \
    "$padding" \
    "$c" \
    "$str" \
    "$c" \
    "$padding"
}

String.format "$@"
```