## Submissões:

* Altair Costa
* Igor Garcia
* José Almeida
* Kretcheu
* Robson Alexandre

## 1. Altair

```
moldura() {
    # moldura  ARG1 ARG2 [de]
    # moldura X teste [de]
    # resultado:
    # XXXXXXXXXXXXX   XXXXXXXXXXXXX   XXXXXXXXXXXXX
    # X teste     X   X   teste   X   X     teste X
    # XXXXXXXXXXXXX   XXXXXXXXXXXXX   XXXXXXXXXXXXX

    # Função de saída em caso de erro
    die() {
        echo "Parâmetros inválidos!" 
        exit 1
    }
    # Verifica se os argumentos foram informados corretamente
    [[ $1 && $2 && ${#1} -eq 1 ]] || die

    # Testa terceiro argumento, para decidir a posição horizontal do texto.
    if [[ $3 == e ]]; then
        string="$1 $2       $1"
    elif [[ $3 == d ]]; then
        string="$1       $2 $1"
    else 
        string="$1    $2    $1"
    fi

    # Monta a linha que será exibida nas partes superior e inferior
    string2="${string//[[:word:][:blank:][:punct:]]/$1}"

    # Impressão do quadro final
    printf "$string2\n$string\n$string2\n"
}
```

### Pontos interessantes:

* Tentou implementar opções de alinhamento
* Utilizou aninhamento de funções para tratar erros

### Problemas:

* Utilzou `exit` para terminar a função
* Variáveis globais permenecem na sessão após uso

### Alternativas:

* Utilizar `return` em vez de `exit` + palavra reservada `local`
* Criar a função com agurpamento com parêntesis (subshell)

---

## 2. Igor

```
function minhafunc {
    num_chars=$((4 + ${#2}))
    intervalo=$(eval echo "{1..$num_chars}")
    printf $1'%.0s' $intervalo
    printf '\n'$1' '$2' '$1'\n'
    printf $1'%.0s' $intervalo
    printf '\n'
}
```

### Pontos interessantes:

* Tentou calcular o número de caracteres da largura do box
* O truque para repetir o caractere de decoração

### Problemas:

* Variáveis globais
* Usou `eval`
* Não funciona com texto com espaços
* _Quoting hell_
* Não trata erros

### Alternativas:

* Palavra reservada `local`
* Utilizar aspas duplas: `printf "\n$1 $2 $1\n"`
* Ver notas sobre as funções do Almeida, do Kretcheu e do Robson

---

## 3. Kretcheu

```
minhafunc () {
   echo -e "$1$1${2//?/$1}$1$1\n$1 ${2} $1\n$1$1${2//?/$1}$1$1"
}
```

### Pontos interessantes:

* Não criou variáveis
* Tudo na expansão de parâmetros

### Problemas:

* Não trata erros
* Usa `echo -e`

### Alternativas:

* Utilizar `printf`

---

## 4. Almeida

```
minhafunc() {
  chr="${1:0:1}" # apenas um caractere
  txt="$chr $2 $chr"
  : "${txt//?/$chr}"

  printf '%s\n' "$_" "$txt" "$_"
}
```

### Pontos interessantes:

* Usou expansões
* O truque da variável especial `$_`

### Problemas:

* Variáveis globais
* Não trata erros

### Alternativas:

* Palavra reservada `local`

---

## 5. Robson

```
function String.format() {
  local c=${1::1} str=$2
  [[ $c ]] && [[ $str ]] || String.help
  length=$((${#str}+4))
  printf -v padding "%${length}s"
  [[ $c == ' ' ]] || padding=${padding// /$c}
  printf '%s\n%s %s %s\n%s\n' \
    "$padding" \
    "$c" \
    "$str" \
    "$c" \
    "$padding"
}
```

### Pontos interessantes:

* Utilizou o `printf` para gerar uma string de espaços da largura do box

### Problemas:

* Não mandou uma _função_, e sim um script
* Tratamento de erros fora da função
* Teste de expressões assertivas duplicado
* Teste de caractere espaço desnecessário

### Alternativas:

* Teste: `[[ ${c// /} && $str ]] || String.help`

```
~ $ a=1; b=
~ $ [[ $a && ${b// /} ]] || echo erro
erro
~ $ a=1; b=' '
~ $ [[ $a && ${b// /} ]] || echo erro
erro
~ $ unset b
~ $ [[ $a && ${b// /} ]] || echo erro
erro
~ $ unset a
~ $ [[ $a && ${b// /} ]] || echo erro
erro
~ $ a=1; b=1
~ $ [[ $a && ${b// /} ]] || echo erro
~ $ 
```
