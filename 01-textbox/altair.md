```
# Função moldura para o desafio de 02/04/2021
moldura() {

# Esta função tem a finalidade de emoldurar um texto,fornecido como
# segundo argumento, usando um único caracter qualquer fornecido como
# primeiro argumento. Caso o texto contenha espaços, ele deverá ser
# informado entre aspas. Foi adicionada a opção de alinhamento à esquerda,
# à direita e centralizado. Como parâmetro adicional, o terceiro,
# pode ser 'e', para esquerda, 'd' para direita e, na falta dessa
# informação, o texto será centralizado.

# moldura  ARG1 ARG2 [de]

# moldura X teste [de]
# resultado:
# XXXXXXXXXXXXX   XXXXXXXXXXXXX   XXXXXXXXXXXXX
# X teste     X   X   teste   X   X     teste X
# XXXXXXXXXXXXX   XXXXXXXXXXXXX   XXXXXXXXXXXXX

# -------------------------------------------------------------------
# Função de saída em caso de erro
    die() {
        echo "Parâmetros inválidos!" 
        exit 1
    }
# -------------------------------------------------------------------
# Verifica se os argumentos foram informados corretamente

[[ $1 && $2 && ${#1} -eq 1 ]] || die

# Monta string, com o caracter fornecido no primeiro argumento
# seguido de três espaços, a string do segundo argumento, três espaços
# e novamente o caracter fornecido. Esses espaços adicionais têm a por
# finalidade melhorar a visualização do deslocamento do texto no quadro.

# Testa terceiro argumento, para decidir a posição horizontal do texto.

if [[ $3 == e ]]; then
        string="$1 $2       $1"
    elif [[ $3 == d ]]; then
        string="$1       $2 $1"
    else 
        string="$1    $2    $1"
fi

# Monta a linha que será exibida nas partes superior e inferior
# da moldura. O seu comprimento é igual ao comprimento do texto
# mais 8 espaços e 2 caracteres. Ela é formada pelo caracter do primeiro
# argumento com a finalidade de gerar uma string com mesmo número
# de caracteres da linha com o texto, de modo que a moldura se adapte ao
# tamanho do texto fornecido.

string2="${string//[[:word:][:blank:][:punct:]]/$1}"

# Impressão do quadro final

printf "$string2\n$string\n$string2\n"
}
```