# Caixa de texto

Crie uma função que, dados um caractere e uma string qualquer, produza a saída abaixo quando chamada:

```
~ $ minhafunc X teste
XXXXXXXXX
X teste X
XXXXXXXXX
```

## Regras

* É uma função, não um script
* Apenas bash (comandos internos)
* Sem loops

## Como apresentar a solução

Envie para blau@debxp.org o link do seu Gist ou Snippet (ou serviços semelhantes) com o código e a explicação detalhada do funcionamento.

## Resultados

Os scripts serão analisados durante a gravação da Sextas Shell do dia 02 de abril, às 19h, pelo Jitsi.

## Prêmio

Além da satisfação pessoal de conseguir resolver um problema, todos nós seremos premiados com mais entendimento sobre o Bash!
