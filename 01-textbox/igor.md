```
function minhafunc {
    num_chars=$((4 + ${#2}))
    intervalo=$(eval echo "{1..$num_chars}")
    printf $1'%.0s' $intervalo
    printf '\n'$1' '$2' '$1'\n'
    printf $1'%.0s' $intervalo
    printf '\n'
}
```

EXPLICAÇÃO DETALHADA:

```
# DECLARA A FUNÇÃO minhafunc
function minhafunc {

# Calcula comprimento das barras superior e inferior
# que corresponde ao comprimento da string mais 4
# (dois de cada lado)

num_chars=$((4 + ${#2}))


# Cria dinamicamente um intervalo de 1 até o
# número de caracteres da largura total do "banner" / quadro

intervalo=$(eval echo "{1..$num_chars}")


########## Impressão da saída ##########

# Imprime barra superior - para isto, usamos um truque:
# concatenamos o caractere desejado com cada um dos elementos 
# do intervalo, porém usamos a função printf para não imprimir
# os elementos do intervalo, formatando a saída para uma largura
# de 0.0 caracteres. Assim somente o primeiro caractere será impresso,
# tantas vezes quanto for o tamanho do intervalo.

printf $1'%.0s' $intervalo


# Imprime a linha formada pelo caractere decorativo, mais um espaço, 
# mais a palavra original, mais um espaço, mais o caractere decorativo

printf '\n'$1' '$2' '$1'\n'


# Imprime a barra inferior exatamente como a superior

printf $1'%.0s' $intervalo


# Imprime nova linha para alinhar a saída do terminal (opcional)

printf '\n'


# Fecha a definição da função

}
```